export const state = () => ({
    uuid: '',
})

export const mutations = {
    uuid: (state, data) => state.uuid = data || '',
}

export const getters = {
    uuid: state => state.uuid,
};

export const actions = {

};
