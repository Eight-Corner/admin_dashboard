// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCGv2SWm2vbwcn8-vawj9-Ri2xfL4xOs0M",
    authDomain: "admin-console-65128.firebaseapp.com",
    projectId: "admin-console-65128",
    storageBucket: "admin-console-65128.appspot.com",
    messagingSenderId: "369867893528",
    appId: "1:369867893528:web:a5ff3a34c77d37d4ac8c6d",
    measurementId: "G-HKSFEVXB6W"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
