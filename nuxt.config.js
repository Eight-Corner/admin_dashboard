export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    target: 'static',
    ssr: false,

    head: {
        title: '관리자 대시보드',
        htmlAttrs: {
            lang: 'ko'
        },
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: "width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no"},
            {hid: 'description', name: 'description', content: ''},
            {name: 'format-detection', content: 'telephone=no'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {
                rel: "stylesheet",
                type: "text/css",
                href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css'
            }
        ],
        script: [
            //    {src : ''}
        ],
    },
    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        'bootstrap/dist/css/bootstrap.css',
        'bootstrap-vue/dist/bootstrap-vue.css',
        '~assets/css/common.css',
        '~assets/css/util.css',
        '~assets/css/bootstrap-custom.css',
        '~assets/font-awesome/css/font-awesome.css',
        '~assets/css/transition.css',
        '~assets/css/options.scss'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '@/plugins/element-ui',

        '~/plugins/alert.js'
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        '@vueuse/nuxt',
        '@nuxt/image',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        'bootstrap-vue/nuxt',
        '@nuxtjs/axios',
        '@nuxtjs/dotenv',
        '@nuxtjs/toast',
        ['vue-sweetalert2/nuxt'],
        ['@nuxtjs/component-cache', {maxAge: 5000}],
        '@nuxtjs/dayjs'
    ],
    sweetalert: {
        confirmButtonColor: '#41b882',
        cancelButtonColor: '#ff7674'
    },

    bootStrapVue: {
        bootstrapCSS: false, // Or `css: false`
        bootstrapVueCSS: false, // Or `bvCSS: false`
    },

    toast: {
        position: 'bottom-center',
        duration: 3000,
        className: 'toast-items',
        containerClass: 'toast-container',
    },

    // dayjs Optional
    dayjs: {
        locales: ['ko'],
        defaultLocale: 'ko',
        defaultTimeZone: 'Asia/Seoul',
        plugins: [
            'utc', // import 'dayjs/plugin/utc'
            'timezone', // import 'dayjs/plugin/timezone'
        ],
    },

    axios: { // axios 설정
        proxy: true,
    },
    cache: {
        // max: 캐시에 사용되는 컴포넌트의 수
        // maxAge: 캐시 시간 (ms)
        max: 1000,
        maxAge: 1000 * 60 * 60
    },

    build: {
        babel: {
            compact: true,
        },
    },

    server: {
        port: 8080, // default: 3000
        host: "0.0.0.0", // default: localhost
        // iosScheme: "nuxtmobile"
    },

    loading: '~/components/Loding.vue'
}
